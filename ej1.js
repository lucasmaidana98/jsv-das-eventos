// // Declarar el vector con 6 elementos de diferentes tipos de datos
let vector = [1, 'dos', true, 4.5, false, 'seis'];

// Imprimir el vector en la consola
console.log('Vector:', vector);

// Imprimir el primer y último elemento del vector usando índices
console.log('Primer elemento:', vector[0]);
console.log('Último elemento:', vector[vector.length - 1]);

// Modificar el valor del tercer elemento
vector[2] = 'tres';

// Imprimir la longitud del vector
console.log('Longitud del vector:', vector.length);

// Agregar un elemento al final del vector usando "push"
vector.push(7);

// Eliminar el último elemento del vector usando "pop" y guardarlo en una variable
let ultimoElemento = vector.pop();
console.log('Último elemento eliminado:', ultimoElemento);

// Agregar un elemento en la mitad del vector usando "splice"
vector.splice(Math.floor(vector.length / 2), 0, 'nuevoElemento');

// Eliminar el primer elemento del vector usando "shift" y guardarlo en una variable
let primerElemento = vector.shift();
console.log('Primer elemento eliminado:', primerElemento);

// Agregar el mismo elemento al inicio del vector usando "unshift"
vector.unshift(primerElemento);

// Imprimir el vector actualizado en la consola
console.log('Vector actualizado:', vector);
