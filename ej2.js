// Declarar el vector con 6 elementos de diferentes tipos de datos
let vector = [1, 'dos', true, 4.5, false, 'seis'];

// a. Imprimir en la consola cada valor usando "for"
console.log('Imprimir usando "for":');
for (let i = 0; i < vector.length; i++) {
  console.log(vector[i]);
}

// b. Imprimir en la consola cada valor usando "forEach"
console.log('Imprimir usando "forEach":');
vector.forEach(function (valor) {
  console.log(valor);
});

// c. Imprimir en la consola cada valor usando "map"
console.log('Imprimir usando "map":');
vector.map(function (valor) {
  console.log(valor);
});

// d. Imprimir en la consola cada valor usando "while"
console.log('Imprimir usando "while":');
let i = 0;
while (i < vector.length) {
  console.log(vector[i]);
  i++;
}

// e. Imprimir en la consola cada valor usando "for..of"
console.log('Imprimir usando "for..of":');
for (let valor of vector) {
  console.log(valor);
}
